\documentclass[a4paper, 12pt]{article}

\usepackage[left=1in, right=1in, bottom=1.1in, top=1.0in]{geometry}  % Set page geometry
\usepackage{graphicx}
\graphicspath{{./res/}}
\usepackage{amsfonts}

\title{Attention-based Sea Surface Height Predictor \\ \large  Preliminary Results Report}
\author{Jeehoon Kang}
\date{}


\begin{document}
    \maketitle
    \pagenumbering{gobble}

    \section{Introduction}
    \paragraph{} Nowadays, extreme weather events and climate change are prevalent and evident. The necessity to accurately model and predict environmental variables become paramount in preparing our future in this world. Climate systems are complex, dynamic systems composed of numerous interconnected variables and are ever-changing in their trends. Recent advancements in high degree-of-freedom models such as the neural network used in deep learning have shown promising results in extracting context from sequential/time-series data, provided that there are enough data to fit the model to convergence. One of the key structures used to model sequence-to-sequence problems is the transformer which uses self-attention to extract relationships between each step of the sequence. This report presents the results of applying a transformer-based neural network to model the sea surface height time series data of 5 sea regions. The aim of the model is to predict sea surface heights of several months into the future given a query of past data.


    \section{Data}
    \paragraph{} We are given monthly recordings of the mean absolute dynamic ocean topography of 5 regions (Global, Korea-Adjacent, East Sea, East-China Sea, Yellow Sea) from the year 1993 to 2021. This is the data sequence of interest, and by modelling the variability and trends in the data, we aim to predict 3 months into the future given a query month and its corresponding past data. In addition, several auxiliary datasets composed of sea surface temperature, air temperature, air pressure, wind speed are given as related climatological variables to the dynamic ocean topography. In this report, only the addition of the sea surface temperature data is explored.

    \section{Methodology}
    \subsection{Model}
    \paragraph{} The general process of the ocean topography predictor is as follows. Given a query date(year and month) \textbf{qdate} and its data, the predictor $f$ takes the following form:
    $$ f: [qdate-N, \ qdate-(N-1), \ ... \ , \  qdate] \rightarrow [qdate+1, \ qdate+2, \ qdate+3] $$

    where $N$ is the number of prior months to use in the prediction. In this report, the default $N$ is set to 6. 

    \paragraph{} As shown above $f$ is a function that maps a sequence to a sequence. The input sequence and the output sequence are not aligned, and the output sequence depends on the information embedded in the entire input sequence. This means that rather than having a simple function that projects the input sequence to the output, it is better to encapsulate the input sequence into a 'context' using some sort of an encoder, and produce the outputs given the context using a decoder. The following diagram shows the transformer-based model used to extract context from the input sequence and iteratively generate output predictions.

    \begin{figure} [h]
        \includegraphics[width=\textwidth]{"attention_sshpredictor.png"}        
    \end{figure}

    \paragraph{} The input is a $N$ month long sequence of data, and at each month are 5 data points corresponding to the sea surface height of the 5 different regions. The addition of sea surface temperature data adds 5 more entries per month. To mitigiate the feature scale difference between the sea surface heights and sea surface temperature, all the input data are z-score normalized per column. The $[N, 10]$ input sequence data is translated into a $[N, 32]$ sequence to seek linear relationships between the sea surface height and temperatures of different regions per month. $32$ is an arbitrary choice that is carried through the model, model parameter size and complexity can be thought of when choosing the model dimensions.
    
    \paragraph{} The $[N, 32]$ sequence is then run through a multi-head self-attention layer to extract relationships between the months. At each time-step, 3 learnable projections, named query, key, value are made. Using a Cinderella analogy, the relationship between Query, Key and Value can be described. When fully trained, the query is like the glass slippers that is used to find the true Cinderella, others may try and fit it, but it won't be perfect. The key and value are the candidates that are trying on the glass slippers. At each time step, a glass slipper is presented in the form of the query, and all of the candidates of every time step presents their foot(the key). By testing how well the foot fits the glass slipper, a score is made. This is the attention score that depicts the time-step importance to the queried time-step. The candidates(values) are then scaled/weighted by those scores going forward. This is repeated for each time-step, and thus a representation of importance or 'fit' between time-steps is made. 

    \paragraph{} By doing this process independently, multiple times, and combining the results afterwards, it becomes a multi-head self-attention layer. In the experiments, we use a 2 head self-attention layer. After the attention layers, a residual connection, linear projection, and normalization is performed similar to the paper that introduced the transformer. This results in the output of the encoder of shape $[N, 32]$. The data here are the weighted sum of values given query for each time step. This output can be considered to capture the cross-column, cross-time relationships latent in the input sequence.

    \paragraph{} The context sequence is then given to the decoder which simply uses it as key and value in an attention calculation against the query being provided from the decoder input. The decoder input starts with the $qdate$ data. The whole structure then means: "Given this month's data, and context information of the previous $N$ months, predict next month's data". The output is then fed back into the decoder input and repeated $M=3$ times to predict $M$ months into the future.

    \subsection{Training}
    \paragraph{} The model above is trained by randomly sampling $N+M$ consecutive months worth of data from the dataset. $N$ months worth of data are inputed to the model, and the resulting $M$ months worth of predictions are simply compared against the ground truth using a mean squared error loss function. This loss is propagated backwards into the model to calculate the respective parameter gradients to the loss. The gradients are then used to update the parameter values in the direction of reducing the loss.

    \paragraph{} The default experiment settings used the Adam optimizer with learning rate 0.005 adjusted by a cosine annealing lr scheduler over 100 epochs.

    \newpage

    \section {Results}
    \subsection{Loss and Validation Loss}

    \paragraph{} The following shows the loss graphs produced while training:

    \begin{figure}[h]
        \includegraphics[width=0.5\textwidth]{"loss.png"}
        \includegraphics[width=0.5\textwidth]{"val_loss.png"}
    \end{figure}

    \paragraph{} As shown from the graphs above, the loss doesn't change much between using different query lengths of 3 months, 6 months, or 12 months. This is not conclusive to say that there is no point of using longer query lengths in predicting the future. Loss values are not representative of the final predictive accuracy. Interestingly, the ablation experiment where the sea surface temperature data was dropped from the input recored a higher loss, and an increasing validation loss as the epochs increased. This is probably due to the fact that the column-wise projection from 5 to 32 presents an over-complex model (compared to the 10 to 32), making it easier to overfit and lose the generalization of the model. It can also suggest that including other climatological data might be beneficial in capturing hidden relationships between each other.
    
    \subsection{Trained region and Test Region tests}
    \paragraph{} The following two graphs have been made from the default model to see if predictions within the trained range, and outside(test) made sense.

    \paragraph{} As seen from the graphs, the trained range test (year 2000) seemed to perform well with most of the trends being captured except for specific spikes. The test case (year 2021), which was not included in training, also performed well, especially in the less variable global and Korea-adjacent regions. Other regions were relatively well predicted, except for the Yellow Sea region which seems to have the greatest variability.

    \begin{figure}
        \includegraphics[width=\textwidth]{ssh_gt_pred_2000.png}
        \caption{Trained Range (2000) test}
    \end{figure}

    \begin{figure}
        \includegraphics[width=\textwidth]{ssh_gt_pred_2021.png}
        \caption{Test Range (2021) test}
    \end{figure}


    \newpage
    \subsection{Differing Query Length}

    \paragraph{} Results for the same test period is also given by models trained with different query lengths.

    \begin{figure}[h]
        \includegraphics[width=\textwidth]{qlen3.png}
        \caption{Query Length = 3 Months}
    \end{figure}

    \begin{figure}[h]
        \includegraphics[width=\textwidth]{qlen12.png}
        \caption{Query Length = 12 Months}
    \end{figure}

    \newpage

    \paragraph{} For all three tested query lengths (3, 6, 12), the Yellow Sea region seemed to be the hardest to predict as the predictions constantly crossed between over-estimation and under estimation. The three month query seems to slightly under-estimate in all the regions, whereas the 12 month region over-estimates or correctly estimates during the spring to summer months.

    \subsection{Ablation test - Sea Surface Temperature}
    \paragraph{} Prediction results with the model that didn't have the sea surface temperature information seems to align with the conclusions derived from the loss graphs.

    \begin{figure}[h]
        \includegraphics[width=\textwidth]{no-sst.png}
        \caption{Trained Range without SST data (2000)}
    \end{figure}

    \begin{figure}[h]
        \includegraphics[width=\textwidth]{no-sst_60ep.png}
        \caption{Test Range without SST data (2021)}
    \end{figure}

    \newpage

     \

    \newpage

    \paragraph{} The trained range graphs show a remarkably well fitted prediction to the ground truth, which is expected when the training itself was trying to get the numbers it was getting as close as it can to the given prediction labels. However the problem is well shown with the test range graph, as even using the best validation loss checkpoint, the results don't seem as good as the predictions made by the model that had access to the sea surface temperature information. 

    \newpage

    \section {Discussion}

    \paragraph{} The experiments and prediction results seem to indicate that the use of a transformer-based neural network in modelling the data sequence and predicting future values of ocean topography data is viable, and that it has the potential to be used alongside with other models in generating interesting remarks about the future. The fact that the model only uses given data (without prior subject knowledge), and fits the data with its near 20 thousand parameters means that it is hard to make 'sense' out of 'how' the model produced the result. However, numerical results like these can still act as a data-based regularizer to other knowledge based models.

    \paragraph{} The training involved random sampling of $N+M$ months of ocean topgoraphy and surface temperature data. The model learned the connections between many different $N$ month periods of data, and would find patterns needed to predict the following three months. This means that the model can learn about the subtle patterns embedded in $N$ month periods of time, but it will never be able to learn a trend or pattern that goes beyond that range. This is a problem as climate data typically not only have local patterns but may include global trends or long-term seasonality. This could probably be resolved by having multiple inputs of different time-spans. Each attention layer can capture the intrinsic relationships in each of the time spans, and a concatenated context feature can be used in predicting the following months. Additionally, learning to predict the parameters of multiple sets of knowledge based models (similar to training to predict the parameters of Gaussian space of embeddings in Variational Auto Encoders) can be a more explainable way of utilizing big data and neural networks while catering for patterns latent or emerging in mutliple periods or long ranges.
    
\end{document}
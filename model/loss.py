import torch.nn as nn


def MSELoss(output, target):
    mse = nn.MSELoss()
    loss = 0
    for i in range(len(output)):
        loss += mse(output[i], target[:, i, :].unsqueeze(1))

    return loss

import math

import torch
import torch.nn as nn

from base import BaseModel

"""
A sea level predictor using Self-Attention
"""


class Attention(nn.Module):
    def __init__(self, feat_dim, qkv_dim, encdec=False):
        super().__init__()
        self.qkv_dim = qkv_dim
        self.encdec = encdec

        self.w_query = nn.Parameter(torch.FloatTensor(feat_dim, qkv_dim))
        if not self.encdec:
            self.w_key = nn.Parameter(torch.FloatTensor(feat_dim, qkv_dim))
            self.w_value = nn.Parameter(torch.FloatTensor(feat_dim, qkv_dim))
            nn.init.xavier_normal_(self.w_key)
            nn.init.xavier_normal_(self.w_value)

        # Initialize weights
        nn.init.xavier_normal_(self.w_query)

    def forward(self, x, keys=None, values=None):
        # Generate Query, Key, Value from input
        queries = torch.matmul(x, self.w_query)
        if not self.encdec:
            keys = torch.matmul(x, self.w_key)
            values = torch.matmul(x, self.w_value)

        # Calculate attention scores (using query and key)
        attn_scores = torch.bmm(queries, keys.permute(0, 2, 1)) * 1 / math.sqrt(self.qkv_dim)
        attn_scores = torch.softmax(attn_scores, dim=2)

        # Get weighted values as output
        weighted_values = torch.mul(values.unsqueeze(1), attn_scores.unsqueeze(2).permute(0, 1, 3, 2))
        output = torch.sum(weighted_values, dim=2)

        return output


class SSHEncoder(nn.Module):
    def __init__(self, seq_len, feat_dim, qkv_dim, heads=2):
        super().__init__()
        # Feature Embedding replacement with a linear layer
        self.emb = nn.Linear(feat_dim, qkv_dim)

        # Multi-head self-attention layer
        self.mh_attn = nn.ModuleList([Attention(qkv_dim, qkv_dim) for _ in range(heads)])

        # Concatenate the multi-head outputs, and reduce to model dimension
        self.concat = nn.Linear(qkv_dim*heads, qkv_dim)

        # Residual connections and layer norms
        self.ln1 = nn.LayerNorm([seq_len, qkv_dim])
        self.ln2 = nn.LayerNorm([seq_len, qkv_dim])

        # Feed forward network replaced with linear layer
        self.linear = nn.Linear(qkv_dim, qkv_dim)

    def forward(self, x):
        # Make input into model dim
        x = self.emb(x)

        # Multi-head attention, concat results
        mh_out = torch.concat([attn(x) for attn in self.mh_attn], dim=-1)

        # Residual skip-connection and layer norm
        normres = self.ln1(self.concat(mh_out) + x)

        # Feed forward and residual skip connection, layer norm
        ffout = self.linear(normres)
        return self.ln2(normres + ffout)


class SSHDecoder(nn.Module):
    def __init__(self, feat_dim, qkv_dim, heads=2):
        super().__init__()
        # Feature Embedding replacement with a linear layer
        self.emb = nn.Linear(feat_dim, qkv_dim)

        # Multi-head Encoder-Decoder Attention
        self.mh_attn = nn.ModuleList([Attention(qkv_dim, qkv_dim, encdec=True) for _ in range(heads)])

        # Concatenate the multi-head outputs, and reduce to model dimension
        self.concat = nn.Linear(qkv_dim * heads, qkv_dim)

        # Residual connections and layer norms
        self.ln1 = nn.LayerNorm([1, qkv_dim])
        self.ln2 = nn.LayerNorm([1, qkv_dim])

        # Feed forward network replaced with linear layer
        self.linear = nn.Linear(qkv_dim, qkv_dim)

        self.outlin = nn.Linear(qkv_dim, feat_dim)

    def forward(self, x, key, value):
        # Make input into model dim
        x = self.emb(x)

        # Multi-head attention, concat results
        mh_out = torch.concat([attn(x, key, value) for attn in self.mh_attn], dim=-1)

        # Residual skip-connection and layer norm
        normres = self.ln1(self.concat(mh_out) + x)

        # Feed forward and residual skip connection, layer norm
        ffout = self.linear(normres)
        return self.outlin(self.ln2(normres + ffout))


class SSHPredictor(BaseModel):
    def __init__(self, seq_len, feat_dim, qkv_dim, prediction_len=3):
        super().__init__()
        self.pred_len = prediction_len
        self.encoder = SSHEncoder(seq_len, feat_dim, qkv_dim)
        self.keylin = nn.Linear(qkv_dim, qkv_dim)
        self.vallin = nn.Linear(qkv_dim, qkv_dim)
        self.decoder = SSHDecoder(feat_dim, qkv_dim)

    def forward(self, x):
        # Get context from prior months
        encout = self.encoder(x)
        key = self.keylin(encout)
        val = self.vallin(encout)

        # Prime the decoder with the last month's data
        primer = x[:, -1, :].unsqueeze(1)

        # Iterate the decoder with the prior months context and recursive outputs
        output = []
        for i in range(self.pred_len):
            primer = self.decoder(primer, key, val)
            output.append(primer)
        return output

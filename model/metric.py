import torch


def chkdiff(output, target):
    return sum(abs(output[0][0][0] - target[0][0][0]))

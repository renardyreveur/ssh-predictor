import pandas as pd
import torch
from torch.utils.data import Dataset


class SSHData(Dataset):
    def __init__(self, query_len: int = 6, pred_len: int = 3, test=False):
        super().__init__()
        # Parameters
        self.qlen = query_len
        self.plen = pred_len
        self.test = test

        # Key Variables:
        key_vars = ['year', 'month', 'GLBmean', 'ASKmean', 'ESmean', 'ECSmean', 'YSmean']

        # Read data files
        sea_level_data = pd.read_csv("data/adt_sla_monthlyMean.csv")
        sea_temp_data = pd.read_csv("data/sst_monthlyMean.csv")

        # Collect key columns
        sea_level_data = sea_level_data[key_vars][sea_level_data['year'] >= 1993]
        sea_temp_data = sea_temp_data[key_vars][sea_temp_data['year'] >= 1993]

        # Create array per month
        sea_data = sea_level_data.set_index(["year", "month"]).join(sea_temp_data.set_index(["year", "month"]), rsuffix="_sst")

        # Normalize each column
        self.column_mean, self.column_std = sea_data.mean(), sea_data.std()

        # (sea_data.index.get_level_values('year') <= 2000) & (sea_data.index.get_level_values('year') >= 1999)
        sea_data = sea_data[sea_data.index.get_level_values('year') >= 2021] if test\
            else sea_data[sea_data.index.get_level_values('year') <= 2020]
        self.normalized_sea_data = (sea_data - self.column_mean) / self.column_std

    def __len__(self):
        return len(self.normalized_sea_data) - (self.qlen + self.plen if not self.test else 0)

    def __getitem__(self, idx):
        idxed_data = self.normalized_sea_data.iloc[idx: idx+self.qlen].to_numpy()
        label = self.normalized_sea_data.iloc[idx+self.qlen: idx+self.qlen + self.plen].to_numpy()
        return torch.from_numpy(idxed_data).float(), torch.from_numpy(label).float()

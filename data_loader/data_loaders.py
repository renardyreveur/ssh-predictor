import numpy as np
from torch.utils.data.dataset import Subset
from torch.utils.data.distributed import DistributedSampler

from base import BaseDataLoader
from data_loader.datasets import SSHData


class SSHDataLoader(BaseDataLoader):
    def __init__(self, query_len, pred_len, batch_size, shuffle=True, validation_split=0.0,
                 distributed=False, rank=0, world_size=1, num_workers=1, prefetch_factor=2):

        self.validation_split = validation_split
        self.rank = rank
        self.world_size = world_size

        # Create dataset instance
        dataset = SSHData(query_len, pred_len)

        # If distributed training, separate train and validation set if validation split given
        # and use Distributed Sampler to manage the data sampling.
        if distributed:
            dataset, sampler, shuffle = self.prep_distributed(dataset)
        else:
            sampler = None

        # Initialize the BaseDataLoader class with the set configuration
        super().__init__(dataset, batch_size, shuffle, validation_split, num_workers,
                         distributed=distributed, sampler=sampler, prefetch_factor=prefetch_factor)

    def prep_distributed(self, data_set):
        idx_full = np.arange(len(data_set))

        np.random.seed(0)
        np.random.shuffle(idx_full)

        len_valid = int(len(data_set) * self.validation_split)

        valid_idx = idx_full[0:len_valid]
        train_idx = np.delete(idx_full, np.arange(0, len_valid))

        train_dataset = Subset(data_set, train_idx)
        valid_dataset = Subset(data_set, valid_idx)

        sampler = DistributedSampler(train_dataset, rank=self.rank, num_replicas=self.world_size, shuffle=True)
        shuffle = False

        data_set = (train_dataset, valid_dataset)
        return data_set, sampler, shuffle

import torch
from data_loader.datasets import SSHData
import plotly.express as px
import pandas as pd
from model import SSHPredictor
import numpy as np
from plotly.subplots import make_subplots

fig = make_subplots(rows=5, cols=1)
dataset = SSHData(query_len=3, test=True)
columns = list(dataset.column_mean.index)
print(f"Possible Queries: \n {columns}")

dfs = []
for query in columns[:5]:
    mean = dataset.column_mean[query]
    std = dataset.column_std[query]

    df = pd.DataFrame({"date": pd.to_datetime([f"2021-{i}" for i in range(1, 13)])})
    df["ground_truth"] = [dataset[i][0][0][columns.index(query)].item()*std + mean for i in range(len(dataset))]
    df["region"] = [query] * 12
    model = SSHPredictor(3, 10, 32)
    model.load_state_dict(torch.load("saved/models/SSHPRED/trial-5-qlen-3/checkpoint-epoch100.pth")['state_dict'])
    model.eval()

    for i in range(len(dataset)):
        print(dataset[i][0].shape[0])
        if dataset[i][0].shape[0] == 3:
            init = [np.nan] * 12
            output = [x[0][0][columns.index(query)].item()*std + mean for x in model(dataset[i][0].unsqueeze(0))]
            init[i+3:6+i] = output
            if not all([np.isnan(x) for x in init[:12]]):
                df[f"pred_{i}"] = init[:12]

    dfs.append(df)

final_df = pd.concat(dfs)

fig = px.line(final_df, x="date", y=final_df.columns, markers=True,
              labels={
                  "value": "Sea Surface Height"
              },
              title=f"Sea Surface Height Ground Truth and Model Prediction Comparison (2021)",
              facet_row="region"
              )
fig.update_yaxes(matches=None)
fig.update_xaxes(
    dtick="M1",
    tickformat="%b\n%Y")

print(final_df.head())

fig.show()


# 수온 빼고 학습
# 과거 기록
# 학습된 부분 확인

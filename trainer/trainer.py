import numpy as np
import torch
import wandb
from torch.cuda.amp import GradScaler, autocast

from base import BaseTrainer
from utils import inf_loop, MetricTracker, get_lr


class Trainer(BaseTrainer):
    """
    Trainer class
    """
    def __init__(self, model, criterion, metric_ftns, optimizer, config, data_loader,
                 valid_data_loader=None, lr_scheduler=None, len_epoch=None, rank=0, world_size=1):
        super().__init__(model, criterion, metric_ftns, optimizer, lr_scheduler, config, rank, world_size)
        self.config = config
        self.data_loader = data_loader
        if len_epoch is None:
            # epoch-based training
            self.len_epoch = len(self.data_loader)
        else:
            # iteration-based training
            self.data_loader = inf_loop(data_loader)
            self.len_epoch = len_epoch
        self.valid_data_loader = valid_data_loader
        self.do_validation = self.valid_data_loader is not None
        self.log_step = int(np.sqrt(data_loader.batch_size))
        self.init_lr = config['optimizer']['args']['lr']

        if 'warm_up' in config['trainer']:
            self.warm_up = config['trainer']['warm_up']
        else:
            self.warm_up = 0

        if config['trainer']['amp']:
            self.scaler = GradScaler()

        if self.rank == 0:
            self.train_metrics = MetricTracker('loss', *[m.__name__ for m in self.metric_ftns])
            self.valid_metrics = MetricTracker('loss', *[m.__name__ for m in self.metric_ftns])

        # dist.barrier()

    def _train_epoch(self, epoch):
        """
        Training logic for an epoch

        :param epoch: Integer, current training epoch.
        :return: A log that contains average loss and metric in this epoch.
        """
        self.model.train()

        if self.rank == 0:
            self.train_metrics.reset()
        # dist.barrier()

        # Set epoch for the DistributedSampler -> shuffle across epochs
        if self.config['distributed']:
            self.data_loader.sampler.set_epoch(epoch)

        lr = 0.0
        for batch_idx, (data, target) in enumerate(self.data_loader):
            if data is None:
                print("Batch is None, skipping...")
                continue

            if self.config['distributed']:
                data, target = data.cuda(self.rank, non_blocking=True), \
                               target.cuda(self.rank, non_blocking=True)
            else:
                data, target = data.to(self.device, non_blocking=True), \
                               target.to(self.device, non_blocking=True)

            # Linear Learning Rate Warm-up
            full_batch_idx = ((epoch-1)*len(self.data_loader) + batch_idx)
            if epoch - 1 < self.warm_up:
                for params in self.optimizer.param_groups:
                    params['lr'] = self.init_lr/(self.warm_up * len(self.data_loader)) * full_batch_idx
            lr = get_lr(self.optimizer)

            # -------- TRAINING LOOP --------
            # self.optimizer.zero_grad()
            for param in self.model.parameters():
                param.grad = None

            if self.config['trainer']['amp']:
                # Auto cast to mixed precision
                with autocast():
                    output = self.model(data)
                    loss = self.criterion(output, target)
                self.scaler.scale(loss).backward()

                # Unscale for gradient clipping
                if self.config['trainer']['grad_clip']:
                    self.scaler.unscale_(self.optimizer)
                    torch.nn.utils.clip_grad_norm_(self.model.parameters(), self.config['trainer']['grad_clip_value'])

                # Update Parameter!
                self.scaler.step(self.optimizer)
                self.scaler.update()
            else:
                # Full Precision Training
                output = self.model(data)
                loss = self.criterion(output, target)
                loss.backward()
                if self.config['trainer']['grad_clip']:
                    torch.nn.utils.clip_grad_norm_(self.model.parameters(), self.config['trainer']['grad_clip_value'])
                self.optimizer.step()
            # -------------------------------

            if self.rank == 0:
                # Update Training Metrics
                self.train_metrics.update('loss', loss.item())
                for met in self.metric_ftns:
                    self.train_metrics.update(met.__name__, met(output, target))

                # Log training progress
                if batch_idx % self.log_step == 0:
                    if self.config['distributed']:
                        print('Train Epoch: {} {} Loss: {:.6f},    lr: {}'.format(
                            epoch,
                            self._progress(batch_idx),
                            loss.item(),
                            get_lr(self.optimizer)
                        ))
                    else:
                        self.logger.debug('Train Epoch: {} {} Loss: {:.6f},    lr: {}'.format(
                            epoch,
                            self._progress(batch_idx),
                            loss.item(),
                            get_lr(self.optimizer)
                        ))
            # dist.barrier()
            if batch_idx == self.len_epoch:
                break

        # Update Learning rate scheduler
        if self.lr_scheduler is not None:
            if type(self.lr_scheduler) == torch.optim.lr_scheduler.ReduceLROnPlateau:
                self.lr_scheduler.step(loss)
            else:
                self.lr_scheduler.step()

        if self.rank == 0:
            # Epoch log
            log = self.train_metrics.result()
            log.update({'lr': lr})

            # Run validation at end of epoch
            if self.do_validation:
                val_log = self._valid_epoch(epoch)
                log.update(**{'val_' + k: v for k, v in val_log.items()})

            # Add log to WandB
            if self.config['track_experiment']:
                wandb.log(log)

            return log

        # dist.barrier()

    def _valid_epoch(self, epoch):
        """
        Validate after training an epoch

        :param epoch: Integer, current training epoch.
        :return: A log that contains information about validation
        """
        self.model.eval()

        if self.rank == 0:
            self.valid_metrics.reset()

        with torch.no_grad():
            for batch_idx, (data, target) in enumerate(self.valid_data_loader):
                data, target = data.to(self.device), target.to(self.device)

                if self.config['trainer']['amp']:
                    with autocast():
                        output = self.model(data)
                        loss = self.criterion(output, target)
                else:
                    output = self.model(data)
                    loss = self.criterion(output, target)

                if self.rank == 0:
                    self.valid_metrics.update('loss', loss.item())
                    for met in self.metric_ftns:
                        self.valid_metrics.update(met.__name__, met(output, target))

            if self.rank == 0:
                return self.valid_metrics.result()
                # dist.barrier()

    def _progress(self, batch_idx):
        base = '[{}/{} ({:.0f}%)]'
        if hasattr(self.data_loader, 'n_samples'):
            total = self.data_loader.n_samples
            current = min(total, batch_idx * self.data_loader.batch_size * self.world_size)
        else:
            current = batch_idx
            total = self.len_epoch
        return base.format(current, total, 100.0 * current / total)
